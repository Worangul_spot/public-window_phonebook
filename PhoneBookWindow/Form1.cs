﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net.Mail;


namespace PhoneBookWindow
{
    public partial class Form1 : Form
    {
        TextBox contentTextBox;
        
        public void CheckMe()
        {
            MessageBox.Show("Uruchomiony");
        }
        public void Resort()
        {
            listBox.Items.Clear();
            IEnumerable<Contact>query=dane.OrderBy(c => c.Name);
            
            foreach (Contact con in query)
            {
                listBox.Items.Add(con.Name + " |=> " + con.Number);
            }
            
        }
        public class Contact
        {
            public string Name { get; set; }
            public string Number { get; set; }
        }
        public List<Contact> dane = new List<Contact>
            {
                new Contact() {Name = "Arkadiusz", Number="123456" },
                new Contact() {Name = "Bartek" , Number="654321" },
                new Contact() {Name = "Cecylia" , Number="741852" },
                new Contact() {Name = "Alibaba" , Number="456789" },
                new Contact() {Name = "Barbara" , Number="258369" }
            };

        public Form1()
        {
            InitializeComponent();
            Resort();
           
            
        }

        private void button1_Click(object sender, EventArgs e)
        {

            if (textBox1.Text != "" && textBox2.Text != "")
            {
                Contact nxC;
                nxC= new Contact() { Name = textBox1.Text, Number = textBox2.Text };
                dane.Add(nxC);
                MessageBox.Show(Text= "Contact with Name: " + nxC.Name + ", and Number: " + nxC.Number + ", is allready added succefully!");
                textBox1.Text = "";
                textBox2.Text = "";
                Resort();
                
            }
            else
            { MessageBox.Show("For Add a Contact, must write a Name and Number.");
                Resort();
            }
        }
        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                var toDelete = listBox.SelectedItem;
                int toDeleteIndex = listBox.SelectedIndex;
                dane.RemoveAt(toDeleteIndex);
                listBox.Items.Remove(toDelete);
            }
            catch { }
        }
        
        private void buttonSend_Click(object sender, EventArgs e)
        {
            CheckMe();
            Form mailSend= new Form2(dane);
            mailSend.Show();
            
        }
    }
}
