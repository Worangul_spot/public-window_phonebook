﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;
using System.Net.Mail;
using static PhoneBookWindow.Form1;
using System.IO;

namespace PhoneBookWindow
{
    public partial class Form2 : Form
    {
        Form1 Form1;
        Form2 mailSend;
        List<Contact> dane2 = new List<Contact>();
        public Form2(List<Contact> dane2)
        {
            InitializeComponent();
            int c = dane2.Count;
            try
            {
                string mailCList = "";
                foreach (Contact con in dane2)
                {
                    mailCList += con.Name + "|=>" + con.Number + Environment.NewLine;
                }
                contentTextBox.Text = mailCList;
            }
            catch(Exception es)
            {
                MessageBox.Show(es.ToString());
            }
        }

        private void buttonOK_Click(object sender, EventArgs e)
        {
            try
            {
               
                MailMessage mail = new MailMessage();
                SmtpClient SmtpServer = new SmtpClient(textBoxClient.Text);
                mail.From = new MailAddress(sendTextBox.Text);
                mail.To.Add(new MailAddress(receiveTextBox.Text));
                mail.Subject="List of PhoneBook Contacs";
               
                mail.Body = contentTextBox.Text;
                mail.BodyEncoding = Encoding.UTF8;
                mail.IsBodyHtml = true;
                mail.Priority = MailPriority.Normal;
                SmtpServer.Port = 587;
                SmtpServer.Credentials = new NetworkCredential(userNameTextBox.Text,passwordTextBox.Text);
                SmtpServer.EnableSsl = true;

                SmtpServer.Send(mail);
                MessageBox.Show("");


            }
            catch (Exception ex )
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
